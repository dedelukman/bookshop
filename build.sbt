name := """bookshop"""
organization := "com.abah"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(guice,javaJdbc)

resolvers ++= Seq(
  "sonatype snapshots".at("https://oss.sonatype.org/content/repositories/snapshots/"),
  "local maven".at(s"file:///${Path.userHome.absolutePath}/.m2/repository")
)


fork := true // required for "sbt run" to pick up javaOptions

javaOptions += "-Dplay.editor=http://localhost:63342/api/file/?file=%s&line=%s"
